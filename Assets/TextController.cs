﻿using TMPro;
using UniRx;
using UnityEngine;

public class TextController : MonoBehaviour
{
    [SerializeField] private Rigidbody planeRigidbody;
    [SerializeField] private TextMeshProUGUI forwardVelocity;
    [SerializeField] private TextMeshProUGUI upwardsVelocity;
    [SerializeField] private TextMeshProUGUI altitudeText;
    [SerializeField] private TextMeshProUGUI rotationText;
    
    void Start()
    {
        planeRigidbody.ObserveEveryValueChanged(rb => rb.velocity)
            .Subscribe(vector3 => forwardVelocity.text = $"Forward velocity : {vector3.x * -1:F1}");
        planeRigidbody.ObserveEveryValueChanged(rb => rb.velocity)
            .Subscribe(vector3 => upwardsVelocity.text = $"Upwards velocity : {vector3.y:F1}");
        planeRigidbody.ObserveEveryValueChanged(rb => rb.transform.rotation.z)
            .Subscribe(rotZ => rotationText.text = $"Rotation : {rotZ:F1}");
        planeRigidbody.ObserveEveryValueChanged(rb => rb.transform.position.y)
            .Subscribe(posY => altitudeText.text = $"Altitude : {posY:F1}");
    }

}
