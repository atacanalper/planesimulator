﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class CrashCheck : MonoBehaviour
{
    [SerializeField] private Collider crashCollider;

    private void Start()
    {
        crashCollider.OnCollisionEnterAsObservable().Subscribe(delegate(Collision collision)
        {
            if (collision.collider.CompareTag("Ground"))
            {
                Debug.Log("Destroyed");
            }
        });
    }
}
