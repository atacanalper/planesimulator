﻿using System;
using UniRx;
using UnityEngine;

public class PlaneController : MonoBehaviour
{
    private bool takeOff;
    private float takeOffThreshold = 250f;

    private bool canFly;
    private float canFlyVelocity = 10f;


    private Rigidbody rb;
    [SerializeField] private float maxForwardVelocity = 450;
    [SerializeField] private float maxUpwardVelocity = 450;
    [SerializeField] private float thrustPower;
    [SerializeField] private float ascendPower;
    [SerializeField] private float descendPower;
    [SerializeField] private float upRotationSpeed;
    [SerializeField] private float downRotationSpeed;
    [SerializeField] private float maxDownRotation = 0.3f;
    [SerializeField] private float maxUpRotation = -0.3f;


    [SerializeField] private ParticleSystem thrustParticle;

    private IDisposable startObservable;
    private IDisposable takeOffObservable;
    private Vector3 rotation;


    private void OnEnable()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        startObservable = Observable.EveryFixedUpdate().Where(l => Input.GetMouseButton(0) && !takeOff).Subscribe(
            delegate
            {
                rb.velocity += transform.right * -thrustPower;
                if (Mathf.Abs(rb.velocity.x) > takeOffThreshold)
                {
                    takeOff = true;
                    startObservable.Dispose();
                    Debug.Log("TakeOff");
                }
            });
        takeOffObservable = Observable.EveryFixedUpdate().Where(l => takeOff).Subscribe(
            delegate
            {
                if (Input.GetMouseButton(0))
                {
                    Thrust();
                    RotateUp();
                }
                else
                {
                    SlowDown();
                    RotateDown();
                }
            });
        Observable.EveryFixedUpdate().Subscribe(delegate(long l)
        {
            if (Input.GetMouseButton(0))
            {
                thrustParticle.Play();
            }
            else
            {
                thrustParticle.Stop();
            }
        });
    }

    private void SlowDown()
    {
        if (rb.velocity.y > -maxUpwardVelocity)
        {
            rb.velocity += new Vector3(0, -descendPower, 0);
        }
    }

    private void Thrust()
    {
        if (rb.velocity.y < maxUpwardVelocity)
        {
            rb.velocity += transform.up * ascendPower;
        }
        if (rb.velocity.x > -maxForwardVelocity)
        {
            rb.velocity += transform.right * -thrustPower;
        }
    }


    private void RotateUp()
    {
        if (transform.rotation.z > maxUpRotation)
        {
            transform.Rotate(0, 0, -upRotationSpeed);
        }
    }

    private void RotateDown()
    {
        if (transform.rotation.z < maxDownRotation)
        {
            transform.Rotate(0, 0, downRotationSpeed);
        }
    }

    private void OnDisable()
    {
        startObservable.Dispose();
        takeOffObservable.Dispose();
    }
}